FROM registry.gitlab.com/multitech-osp/dockerfiles/php/8.1-fpm-alpine-nginx

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions xdebug

COPY ./php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

EXPOSE 9000 9003
